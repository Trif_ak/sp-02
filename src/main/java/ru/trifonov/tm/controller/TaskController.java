package ru.trifonov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.trifonov.tm.api.IProjectService;
import ru.trifonov.tm.api.ITaskService;
import ru.trifonov.tm.model.Project;
import ru.trifonov.tm.model.Task;

import java.util.List;

@Controller
public final class TaskController {
    @NotNull
    ITaskService taskService;
    @NotNull
    IProjectService projectService;

    @Autowired
    public TaskController(@NotNull final ITaskService taskService, @NotNull final IProjectService projectService) {
        this.taskService = taskService;
        this.projectService = projectService;
    }

    @NotNull
    @PostMapping("/task-create")
    public String createTaskPost(
//            Model model,
            @ModelAttribute("projectId") @Nullable final String projectId,
            @ModelAttribute("name") @Nullable final String name,
            @ModelAttribute("description") @Nullable final String description,
            @ModelAttribute("beginDate") @Nullable final String beginDate,
            @ModelAttribute("endDate") @Nullable final String endDate
    ) {
        taskService.insert(projectId, name, description, beginDate, endDate);
//        findTaskByProjectId(model, projectId);
        return "redirect:/tasks";
    }

    @NotNull
    @GetMapping("/task-create")
    public String createTaskGet(
            @NotNull final Model model,
            @RequestParam(name = "projectId") final String projectId
    ) {
        model.addAttribute("projectId", projectId);
//        model.addAttribute("statuses", Arrays.asList(CurrentStatus.values()));
        return "taskCreate";
    }

    @NotNull
    @PostMapping("/task-edit")
    public String taskEditPost(
            @ModelAttribute("id") @Nullable final String id,
            @ModelAttribute("projectId") @Nullable final String projectId,
            @ModelAttribute("name") @Nullable final String name,
            @ModelAttribute("description") @Nullable final String description,
            @ModelAttribute("beginDate") @Nullable final String dateOfBegin,
            @ModelAttribute("endDate") @Nullable final String dateOfEnd
    ){
        taskService.update(id, projectId, name, description, dateOfBegin, dateOfEnd);
        return "redirect:/tasks";
    }

    @NotNull
    @GetMapping("/task-edit")
    public String projectEdit(
            @NotNull final Model model,
            @RequestParam(name = "id") final String id,
            @RequestParam(name = "projectId") final String projectId
    ) {
        System.out.println(id);
        System.out.println(projectId);
        model.addAttribute("id", id);
        model.addAttribute("projectId", projectId);
//        model.addAttribute("statuses", Arrays.asList(CurrentStatus.values()));
        return "taskEdit";
    }

    @NotNull
    @GetMapping("/tasks")
    public String findAll(@NotNull final Model model) {
        @NotNull final List<Task> tasks = taskService.findAll();
        model.addAttribute("taskList", tasks);
        return "tasks";
    }

    @NotNull
    @GetMapping("/tasks-of-project")
    public String findTaskByProjectId(@NotNull final Model model, @RequestParam(name = "projectId") @Nullable final String projectId) {
        @NotNull final List<Task> tasks = taskService.findAllByProjectId(projectId);
        model.addAttribute("taskList", tasks);
        model.addAttribute("projectId", projectId);
        return "tasksOfProject";
    }

    @NotNull
    @GetMapping("/task-view")
    public String findById(@NotNull final Model model, @RequestParam(name = "id") @Nullable final String id) {
        @NotNull final Task task = taskService.find(id);
        model.addAttribute("task", task);
        return "taskView";
    }

    @NotNull
    @GetMapping("/task-delete")
    public String delete(@RequestParam(name = "id") @Nullable final String id) {
        taskService.delete(id);
        return "redirect:/tasks";
    }
}
