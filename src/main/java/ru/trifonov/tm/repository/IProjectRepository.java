package ru.trifonov.tm.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.trifonov.tm.model.Project;

import java.util.List;

@Repository
public interface IProjectRepository extends CrudRepository<Project, String> {
    @Override
    List<Project> findAll();
}
