<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <title>PROJECT LIST</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a style="font-size: 26px" class="navbar-brand mb-0 h1" href="/">PROJECT_MANAGER</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div style="margin-left: 30px" class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a style="font-size: 20px" class="nav-link" href="/">HOME</a>
            </li>
            <li class="nav-item">
                <a style="font-size: 20px; margin-left: 10px" class="nav-link" href="/projects">PROJECTS</a>
            </li>
            <li class="nav-item">
                <a style="font-size: 20px; margin-left: 10px" class="nav-link" href="/tasks">TASKS</a>
            </li>
        </ul>
    </div>
</nav>
<table class="table">
    <thead class="thead-dark">
    <tr>
        <th scope="col">#</th>
        <th scope="col">NAME</th>
        <th scope="col">DESCRIPTION</th>
        <th scope="col">ID</th>
        <th scope="col">TASK</th>
        <th scope="col">VIEW</th>
        <th scope="col">EDIT</th>
        <th scope="col">REMOVE</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="project" items="${projectList}">
        <tr>
            <td></td>
            <td>${project.name}</td>
            <td>${project.description}</td>
            <td>${project.id}</td>
            <td>
                <a href="/tasks-of-project/?projectId=${project.id}" class="btn btn-dark btn-sm active" role="button" aria-disabled="true">VIEW TASK</a>
            </td>
            <td>
                <a href="/project-view/?id=${project.id}">VIEW</a>
            </td>
            <td>
                <a href="/project-edit?id=${project.id}">EDIT</a>
            </td>
            <td>
                <a href="/project-delete?id=${project.id}">REMOVE</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<a style="margin-left: 20px; margin-top: 5px" href="/project-create" class="btn btn-success active" role="button" aria-disabled="true">CREATE PROJECT</a>
<a style="margin-left: 10px; margin-top: 5px" href="/" class="btn btn-danger active" role="button" aria-disabled="true">BACK</a>
<a style="margin-left: 10px; margin-top: 5px" href="/projects" class="btn btn-secondary active" role="button" aria-disabled="true">REFRESH</a>
</body>
</html>